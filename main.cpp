///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include "main.hpp"
#include <array>
#include <list>

using namespace std;
using namespace animalfarm;

int Animal::length;
int Animal::randAlphabetIndex; 
char Animal::randomName[10];
char Animal::alphabet[25];
int Animal::i;

int main() {
  
   srand( time(NULL) );

   //Create array container and initialize elements to NULL
   array<Animal*, 30> animalArray;
   animalArray.fill(NULL);

   //Fill container with random Animals
   for (int i = 0 ; i < 25 ; ++i ) {
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   //Print using array array methods
   cout << "Array of Animals:" << endl;
   cout << "   Is it empty: " << (animalArray.empty() ? "true" : "false") << endl;
	cout << "   Number of elements: " << animalArray.size()  << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;

   //Iterate over the array
   for ( array<Animal*, 30>::iterator it = animalArray.begin() ; it < animalArray.end() ; it++ )
   {
      if (*it != NULL) {
      cout << (*it)->speak() << endl; 
      }
      else 
         break;
   }

   //Create list container and initialize elements
   list<Animal*> animalList;

   //Fill container with random Animals
   for (int k = 0 ; k < 25 ; k++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
   }

   //Print using list methods
   cout << "List of Animals: " << endl;
   cout << "   Is it empty: " << (animalList.empty() ? "true" : "false") << endl;
   cout << "   Number of elements: " << animalList.size() << endl;
   cout << "   Max size : " << animalList.max_size() << endl;

   //Iterate over the list
   for ( list<Animal*>::iterator it = animalList.begin() ; it != animalList.end() ; it++ )
   {
      cout << (*it)->speak() << endl;
   }

   //Free allocated memory
   for ( Animal* animal : animalArray ) {
      delete animal;
   }

   for ( Animal* pAnimal : animalList ) {
      delete pAnimal;
   }

   return 0;
}
