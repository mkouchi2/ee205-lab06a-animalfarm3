///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   21_FEB_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "fish.hpp"

using namespace std;

namespace animalfarm{

class Nunu : public Fish {
      public:
         //Class Members & Functions
         bool isNative;
      
         void printInfo();

         //Class Declaration
         Nunu( bool newNative, enum Color newColor, enum Gender newGender  );
      
};

} //namespace animalfarm
