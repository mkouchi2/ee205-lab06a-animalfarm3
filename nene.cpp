///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23_FEB_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>

#include "nene.hpp"

using namespace std;

namespace animalfarm {
   
Nene::Nene( string newTag, enum Color newColor, enum Gender newGender ){
      species = "Branta sandvicensis";
      isMigratory = true;
      featherColor = newColor;
      gender = newGender;
      tagID = newTag;
}

const string Nene::speak(){
      return("Nay, nay");
}

void Nene::printInfo(){
     cout << "Nene" << endl;
     cout << "   Tag ID = [" << tagID  << "]" << endl;
     Bird::printInfo();
}

}


