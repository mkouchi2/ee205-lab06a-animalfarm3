///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>
#include <ctime>
#include <cstdlib>


namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN }; 

class Animal {

private:
   // Variable declaration
   static int length; 
   static int randAlphabetIndex; 
   static char randomName[10];
   static char alphabet[25];
   static int i;

public:
	enum Gender gender;
	string      species;
	
   //Speak Method
   virtual const string speak() = 0;
	
	void printInfo();
	
	static string colorName  (enum Color color);
	static string genderName (enum Gender gender);

   //Random Attribute Method Declarations
   static const Gender getRandomGender();
   static const Color getRandomColor();
   static const bool getRandomBool();
   static const float getRandomWeight( const float from, const float to );
   static const string getRandomName();    

   //Declare Constructors for Animal objects
   Animal();  
   ~Animal();

};

class AnimalFactory {

public:
   static Animal* getRandomAnimal();

};

} // namespace animalfarm
