///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_FEB_2021 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "main.hpp"

using namespace std;

namespace animalfarm {

Animal::Animal() {
  cout << "." << endl;
}

Animal::~Animal() {
  cout << "x" << endl;
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
		case BLACK: return string("Black"); break;
		case WHITE: return string("White"); break;
		case RED: return string("Red"); break;
		case SILVER: return string("Silver"); break;
		case YELLOW: return string("Yellow"); break;
		case BROWN: return string("Brown"); break;
	}
   return string("Unknown");
};


const string Animal::getRandomName(){

   //Initialize Variables
   length = rand()%6+4; //Generate name length between 4-9 letter
   randAlphabetIndex = rand() % 25; //generate # betwen 0-25
   
   // Fill array with A-Z chars
   for (i = 0; i<25; i++){
      alphabet[i] = {static_cast<char>(65+i)};
   }

   // Create Random Name
   randomName[0] = alphabet[randAlphabetIndex];
   for (i=1; i<length ;i++){
      randAlphabetIndex = rand() % 25; // Re-randomize the alphabet index
      randomName[i] = tolower( alphabet[randAlphabetIndex] );
   }
   // Convert char array to string
   string sName(randomName);

   return sName;
}

const Gender Animal::getRandomGender() {
   
   Gender randomGender = static_cast<Gender>( rand()%3 );
	return randomGender;
}

const Color Animal::getRandomColor() {
   

   Color randomColor = static_cast<Color>( rand()%6 );
   return randomColor;
}

const bool Animal::getRandomBool() {

   
   return( rand() % 2 );
}


const float Animal::getRandomWeight( const float from, const float to ) {
   
   float randomWeight = from + static_cast<float>( rand() ) / static_cast<float>( RAND_MAX / ( to - from) );
   return randomWeight;
}

Animal* AnimalFactory::getRandomAnimal() {
   
   Animal* newAnimal = NULL;
   int i;
   i = rand() % 6; // This gives you a number from 0 to 5
   
   switch (i) {
      case 0: newAnimal = new Cat ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
              return newAnimal;
      case 1: newAnimal = new Dog ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
              return newAnimal;
      case 2: newAnimal = new Nunu ( Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender() );
              return newAnimal;
      case 3: newAnimal = new Aku ( Animal::getRandomWeight(5.0, 20.0), Animal::getRandomColor(), Animal::getRandomGender() );
              return newAnimal;
      case 4: newAnimal = new Palila( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
              return newAnimal;
      case 5: newAnimal = new Nene ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); 
              return newAnimal;
   }
   return newAnimal;
}

}// namespace animalfarm
